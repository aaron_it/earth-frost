package vip.justlive.frost.core.job;

import java.io.Serializable;
import java.util.Objects;
import lombok.NoArgsConstructor;
import vip.justlive.frost.api.model.JobExecuteParam;
import vip.justlive.frost.api.model.JobInfo;
import vip.justlive.frost.core.util.ScriptJobFactory;
import vip.justlive.oxygen.core.exception.Exceptions;

/**
 * script模式job包装
 *
 * @author wubo
 */
@NoArgsConstructor
public class JobScriptExecuteWrapper extends AbstractJobExecuteWrapper implements Serializable {

  private static final long serialVersionUID = 1L;

  public JobScriptExecuteWrapper(JobExecuteParam jobExecuteParam) {
    this.jobExecuteParam = jobExecuteParam;
    this.jobExecuteParam.setExecuteAt(time());
  }

  @Override
  public void doRun() {
    this.before();
    if (!Objects.equals(jobInfo.getType(), JobInfo.TYPE.SCRIPT.name())) {
      throw Exceptions.fail("30002", "执行job类型不匹配");
    }
    BaseJob job = getJob();
    job.execute(new DefaultJobContext(jobInfo, jobExecuteParam));
  }

  @Override
  protected BaseJob getJob() {
    return ScriptJobFactory.parse(jobInfo.getScript());
  }

}
